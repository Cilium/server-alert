FROM python:3.8-alpine

ENV PYTHONUNBUFFERED 1
ENV TERM=xterm

RUN mkdir /heartbeat
COPY . /heartbeat
WORKDIR /heartbeat


RUN pip install -r requirements.txt
