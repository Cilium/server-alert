# Server Alert
Email alerts if your server goes down.

## Config
Make a config.yml file based config_template.yml

## Run
To run the tool:

```
docker-compose up
```

## License

This project is licensed under the Apache License - see the [LICENSE](LICENSE) file for details


