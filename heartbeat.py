import schedule
import time
import yaml
import os
import sys

import utils.check_server as cs
import utils.config as config

def main():
    print("Starting monitor...")
    print("Loading config.yml")
    yml_config = config.YamlConfig("config.yml").load()
    
    if yml_config is None:
        print("An error occurred, exiting...")
        sys.exit(-1)

    targets = yml_config["targets"]
    alerts = yml_config["alerts"]
    for target_name, target in targets.items():
        alert = target["alert"]["use"]
        alert_config = alerts[alert]

        check_server = cs.CheckServer(target_name, target, alert_config)
        schedule.every(target["every"]).seconds.do(check_server)
    
    print("Running")
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == "__main__":
    main()
