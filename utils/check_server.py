import requests
import enum
from http import HTTPStatus

import utils.send_email as se

class ServerStateEnum(enum.Enum):
    UP = "up"
    UNREACHABLE = "unreachable"
    DOWN = "down"
    ERROR = "error"

class CheckServer:
    def __init__(self, name, target, alert):
        self.name = name
        self.target = target
        
        if self.target["resource"].startswith("/"):
            self.target["resource"] = self.target["resource"][1:]
        
        self.uri = f"http://{self.target['url']}:{self.target['port']}/{self.target['resource']}"
        self.alert = alert

        self.state = ServerStateEnum.UP
        self.code = 200
        self.retries = 0

        self.email_sender = se.SendEmail(
            host=self.alert["email_host"],
            user=self.alert["email_user"],
            pwd=self.alert["email_pwd"],
        )
        self.destination_emails = self.target["alert"]["to"]

    def __check(self):
        send_notification = False

        try:
            r = requests.get(self.uri, timeout=10)
            if r.status_code != self.target["expected_code"]:
                send_notification = self.state != ServerStateEnum.ERROR
                self.state = ServerStateEnum.ERROR
            else:
                send_notification = self.state != ServerStateEnum.UP
                self.state = ServerStateEnum.UP
            
            self.retries = 0
            self.code = r.status_code
        except requests.exceptions.ConnectionError:
            self.retries += 1
            if self.state != ServerStateEnum.DOWN:
                self.state = ServerStateEnum.UNREACHABLE

            self.code = -1

            if self.retries >= self.alert["retries"]:
                send_notification = self.state != ServerStateEnum.DOWN
                self.state = ServerStateEnum.DOWN
            else:
                send_notification = False


        if self.code > 0:
            phrase = HTTPStatus(self.code).phrase
            print(f"[{self.name}]", self.uri, self.state.value.upper(), f"({self.code} {phrase})")
        else:
            print(f"[{self.name}]", self.uri, self.state.value.upper())

        if send_notification:
            template = """Hello, this is the automated alert service.
[[replace]]

Thank you.
Do not reply to this message."""

            if self.state == ServerStateEnum.UP:
                self._send_notification(
                    self.name,
                    template.replace(
                        "[[replace]]",
                        f"I inform you that your service '{self.name}' ({self.uri}) has returned online after being down."
                    )
                )
            elif self.state == ServerStateEnum.ERROR:
                phrase = HTTPStatus(self.code).phrase
                self._send_notification(
                    self.name,
                    template.replace(
                        "[[replace]]",
                        f"I inform you that your service '{self.name}' ({self.uri}) returned an error status code ({self.code} {phrase})."
                    )
                )

            else:
                s = self.retries * self.target["every"]
                ms = self._sec_to_min(s)
                self._send_notification(
                    self.name,
                    template.replace(
                        "[[replace]]",
                        f"I inform you that your service '{self.name}' ({self.uri}) is currently DOWN from {ms[0]} min and {ms[1]} sec."
                    )
                )

    def _sec_to_min(self, s):
        minutes = s // 60
        seconds = s % 60

        return minutes, seconds

    def _send_notification(self, service_name, message):
        for to_email in self.destination_emails:
            self.email_sender.send_email(
                to_email=to_email,
                subject=f"Alert about service '{service_name}'",
                body=message
            )

    def __call__(self):
        self.__check()
