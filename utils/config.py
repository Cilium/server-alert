import os.path
import yaml

class ConfigException(Exception):
    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)

class YamlConfig:
    def __init__(self, yaml_file_path):
        self.yaml_file_path = yaml_file_path

    def load(self):
        config = self._load_yml()

        try:
            self._check_format(config)
        except ConfigException as e:
            print(str(e))
            return None

        return config

    def _load_yml(self):
        if os.path.isfile(self.yaml_file_path):   # if exists
            with open(self.yaml_file_path, 'r') as config_file:
                config = yaml.safe_load(config_file)
                return config

        return None

    def _check_format(self, config):
        self._error("targets" in config, "'targets' key not in config")
        self._error("alerts" in config, "'alerts' key not in config")

        alerts = self._check_alerts(config["alerts"])
        self._check_targets(config["targets"], alerts)

    def _check_targets(self, targets, alerts):
        for k, v in targets.items():
            self._error("url" in v, "'url' not in config")
            self._error("alert" in v, "'alert' not in config")
            self._error("use" in v["alert"], "'use' not in config")
            self._error("to" in v["alert"], "'to' not in config")
            self._error(v["alert"]["use"] in alerts, f"Alert \'{v['alert']}\' not registered")

            if "port" in v:
                self._error(self._is_integer(v["port"]), "'port' is not integer")
            else:
                targets[k]["port"] = 80

            if "every" in v:
                self._error(self._is_integer(v["every"]), "'every' is not integer")
                self._error(v["every"] >= 1, "'every' must be > 1")
            else:
                targets[k]["every"] = 60

            if "expected_code" in v:
                self._error(self._is_integer(v["expected_code"]), "'expected_code' is not integer")
                self._error(v["expected_code"] > 0, "'expected_code' must be > 0")
            else:
                targets[k]["expected_code"] = 200

    def _check_alerts(self, alerts):
        al = []

        for k, v in alerts.items():
            self._error("type" in v)
            self._error("email_host" in v)
            self._error("email_user" in v)
            self._error("email_pwd" in v)

            if "retries" in v:
                self._error(self._is_integer(v["retries"]), "'retries' is not integer")
                self._error(v["retries"] > 0, "'retries' must be > 0")
            else:
                alerts[k]["retries"] = 5

            al.append(k)

        return al

    def _error(self, assertion, msg="Format invalid"):
        if not assertion:
            raise ConfigException(msg)

    def _is_integer(self, v):
        try:
            int(v)
            return True
        except Exception:
            return False
