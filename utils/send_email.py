import smtplib, ssl

class SendEmail:
    port = 587
    def __init__(self, host, user, pwd):
        self.host = host    #e.g smtp.gmail.com
        self.user = user    #e.g my@gmail.com
        self.pwd = pwd

    def send_email(self, to_email, subject, body):
        context = ssl.create_default_context()

        with smtplib.SMTP(self.host, SendEmail.port) as server:
            server = smtplib.SMTP(self.host, SendEmail.port)
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            server.login(self.user, self.pwd)

            server.sendmail(self.user, to_email, self._make_message(subject, body))

    def _make_message(self, subject, body):
        return 'Subject: {}\n\n{}'.format(subject, body)
